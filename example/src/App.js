import React from 'react'

import { ExampleComponent ,Network} from 'dma-common-ui'
import 'dma-common-ui/dist/index.css'

const App = () => {

  Network.requestGetWidthHeaders({},'https://dmard.beebuddy.net/DMA5backend/app/version').then((data)=>{
    console.log(data)
  }).catch((error)=>{
    console.error(error)
  })

  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
