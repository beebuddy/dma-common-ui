# dma-common-ui

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/dma-common-ui.svg)](https://www.npmjs.com/package/dma-common-ui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save dma-common-ui
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'dma-common-ui'
import 'dma-common-ui/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
