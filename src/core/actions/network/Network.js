import axios from 'axios'
import {
  AxiosResponse,
  AxiosRequestConfig
} from 'axios'
import FormData from 'form-data'


const TAG = "[Network]"

const handleNetworkError = (error) => {
  
}
class Network {

  constructor() {
  }


  /**
   * @returns {AxiosRequestConfig}
   */
  getNetworkConfig() {
    /**
    * @type {AxiosRequestConfig}
     */
    let config = {
      timeout: 1000 * 60 * 2,
      headers: {
        "DMA-Device-Key": this.DeviceId,
        // "Application-Key": deployConfig.applicationId,
        // "SG-APPLICATIONID": deployConfig.SG_APPLICATIONID
      }
    }

    return config

  }

  /**
   * 
   * @param {JSON} body 
   * @param {string} url 
   * @param {AxiosRequestConfig} customConfig
   * @returns {AxiosResponse}
   */
  static async requestPost(body, url, customConfig = null) {
    let _TAG = TAG + '[requestPost]'
    try {

      /**
       * @type {customConfig}
       */
      let config = {
        timeout: 1000 * 60 * 30,
      };
      if (customConfig != null) {
        config = { ...config, ...customConfig }
        console.log(_TAG, "Merge config", config)
      }

      const result = await axios.post(url, body, config);

      console.log(_TAG, "NETWORK.REQUEST success : ", result);

      if (result?.status == 200) {
        return result;
      } else {
        throw result
      }
    } catch (error) {
      console.log(_TAG, "NETWORK.REQUEST error : ", error);
      console.log(_TAG, "NETWORK.REQUEST error status : ", error?.response?.status);
      throw {
        errorFrom: COMMON_CONST.ERROR_CODE.ERROR_FROM_NETWORK,
        message: error.message,
        status: error.response ? error.response.status : "",
        data: error.response ? error.response.data : "",
        response: {
          headers: error.response ? error.response.headers : ""
        },
        errorConfig: error.config

      }
    }
  }

  /**
   * @param {string} url 
   * @param {JSON} body 
   * @param {JSON} headers
   * @returns {AxiosPromise}
   */
  static requestPostWidthHeaders(url, body, headers) {
    let config = {
      timeout: 1000 * 60 * 2,
    }
    config.headers = {
      ...headers,
      "Content-Type": "application/json"
    }

    console.log("headers => ", config);
    console.log("body => ", body);
    console.log("url => ", url);
    return axios.post(url, body, config).then((result) => {
      console.log("NETWORK.REQUEST success : ", result);
      return result;
    }).catch((error) => {
      console.log("NETWORK.REQUEST error : ", error);
      throw {
        errorFrom: COMMON_CONST.ERROR_CODE.ERROR_FROM_NETWORK,
        message: error.message,
        status: error.response ? error.response.status : "",
        data: error.response ? error.response.data : "",
        response: {
          headers: error.response ? error.response.headers : ""
        },
        errorConfig: error.config

      };
    });
  }

  /**
  * 
  * @param {JSON} headers 
  * @param {string} url 
  * @returns {AxiosPromise}
  */
  static requestGetWidthHeaders(headers, url) {
    let config = {
      timeout: 1000 * 60 * 2,
    }
    config.headers = {
      ...headers,
      "Content-Type": "application/json"
    }


    console.log("headers => ", config);
    console.log("url => ", url);
    return axios.get(url, config).then((result) => {
      console.log("NETWORK.REQUEST success : ", result);
      return result;
    }).catch((error) => {
      console.log("NETWORK.REQUEST error : ", error);
    });
  }

  /**
  * 
  * @param {JSON} headers 
  * @param {string} url 
  * @param {FormData} data 
  * 
  * @returns {AxiosPromise}
  */
  static requestPostFormDataWidthHeaders(headers, data, url) {
    let config = {
      timeout: 1000 * 60 * 2,
    }
    config.headers = {
      ...headers,
      'accept': 'application/json',
      'Accept-Language': 'en-US,en;q=0.8',
      'Content-Type': 'multipart/form-data',
    }


    console.log("headers => ", config);
    console.log("url => ", url);
    console.log("data => ", data);
    return axios.post(url, data, config).then((result) => {
      console.log("NETWORK.REQUEST success : ", result);
      return result;
    }).catch((error) => {
      console.log("NETWORK.REQUEST error : ", error);
      throw {
        errorFrom: COMMON_CONST.ERROR_CODE.ERROR_FROM_NETWORK,
        message: error.message,
        status: error.response ? error.response.status : "",
        data: error.response ? error.response.data : "",
        response: {
          headers: error.response ? error.response.headers : ""
        },
        errorConfig: error.config

      };
    });
  }


  /**
 * 
 * @param {JSON} headers 
 * @param {string} url 
 * @param {FormData} data 
 * 
 * @returns {AxiosPromise}
 */
  static requestPostFormDataDownload(headers, data, url) {
    let config = {
      timeout: 1000 * 60 * 2,
    }
    config.headers = {
      ...headers,
      'accept': 'application/json',
      'Accept-Language': 'en-US,en;q=0.8',
      'Content-Type': 'multipart/form-data',
    }
    config.responseType = "arraybuffer"



    console.log("headers => ", config);
    console.log("url => ", url);
    console.log("data => ", data);
    return axios.post(url, data, config).then((result) => {
      console.log("NETWORK.REQUEST success : ", result);
      return result;
    }).catch((error) => {
      console.log("NETWORK.REQUEST error : ", error);
      throw {
        errorFrom: COMMON_CONST.ERROR_CODE.ERROR_FROM_NETWORK,
        message: error.message,
        status: error.response ? error.response.status : "",
        data: error.response ? error.response.data : "",
        response: {
          headers: error.response ? error.response.headers : ""
        },
        errorConfig: error.config

      };
    });
  }


  /**
   * 
   * @param {JSON} body 
   * @param {string} url 
   * @returns {AxiosPromise}
   */
  static requestGet(body, url) {
    return axios.get(url, body);
  }

}

export default Network;

const COMMON_CONST = {
  ERROR_CODE: {
    ERROR_FROM_NETWORK: "ERROR_FROM_NETWORK",
    ERROR_FROM_SCREEN_INPUT: "ERROR_FROM_SCREEN_INPUT",
    ERROR_FROM_SCREEN_OUTPUT: "ERROR_FROM_SCREEN_OUTPUT",
    ERROR_FROM_APIOUTPUT: "ERROR_FROM_APIOUTPUT"
  }
}