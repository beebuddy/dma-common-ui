
const TAG = "[Global]";
const PRIVATE_KEY_PREFIX = "$_$!_"

const myStorage = sessionStorage
class Global {

   /*  constructor() {

    }
 */
    static convertToString(value, defaultValue = '') {
        try {
            return value ? value + '' : '';
        } catch (error) {
            return defaultValue;
        }
    }

    static setPrivateGlobal(key, value) {
        this.setGlobal(PRIVATE_KEY_PREFIX + key, value)
    }

    static getPrivateGlobal(key) {
        return this.getGlobal(PRIVATE_KEY_PREFIX + key);
    }

    static async clearPrivateGlobal(key) {
        let _TAG = TAG + '[clearPrivateGlobal] '
        if (key) {
            await myStorage.removeItem(PRIVATE_KEY_PREFIX + key)
        } else {
            throw new Error(`${_TAG} private key must not be undefined or null`)
        }

    }


    static getAll() {
        console.log(`${TAG} Global getAll()`);
        try {
            return {...myStorage};

        } catch (error) {
            console.log(`${TAG} Global getAll() Error ! \n`, error);
            throw error;
        }
    }
    static async clearAll() {
        const globalData = await this.getAll();
        for(let key in globalData){
          if(key.indexOf(PRIVATE_KEY_PREFIX) === 0){
            // Do Nothing
          } else {
            await myStorage.removeItem(key);
          }
        }
      }

    /**
     * 
     * @param {String} key 
     * @param {String} value 
     * 
     */
    static setGlobal(key, value) {

        let _TAG = TAG + '[setGlobal] '
        console.log(`${_TAG} Set Global => ${key} : ${value}`);
        if (!key) {
            throw Error(`${_TAG} key must not be undefined or null`)
        }
        myStorage.setItem(key, this.convertToString(value));

    }

    /**
     * 
     * @param {String} key 
     * @returns {String}
     */
    static getGlobal(key) {
        return myStorage.getItem(key);
    }


}

export default Global;