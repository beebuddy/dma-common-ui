import { Component } from 'react'
class Params {

    /**
     * 
     * @param {Component} reactComponent 
     */
    static getParams(reactComponent){
        
        return reactComponent.props.location.state;
    }

}

export default Params