
import {
  AxiosResponse,
  AxiosRequestConfig
} from 'axios'

export class Network {
  /**
   * @returns {AxiosRequestConfig}
   */
  getNetworkConfig() :AxiosRequestConfig;

  /**
   * @description เรียกแบบ post
   */
  static requestPost(body:JSON, url:String, customConfig?: AxiosRequestConfig):Promise<AxiosResponse>;

  /**
   * @param {string} url 
   * @param {JSON} body 
   * @param {JSON} headers
   * @returns {AxiosPromise}
   */
  static requestPostWidthHeaders(url, body, headers) {

  }

  /**
  * 
  * @param {JSON} headers 
  * @param {string} url 
  * @returns {AxiosPromise}
  */
  static requestGetWidthHeaders(headers, url) {

  }

  /**
  * 
  * @param {JSON} headers 
  * @param {string} url 
  * @param {FormData} data 
  * 
  * @returns {AxiosPromise}
  */
  static requestPostFormDataWidthHeaders(headers, data, url) {

  }


  /**
 * 
 * @param {JSON} headers 
 * @param {string} url 
 * @param {FormData} data 
 * 
 * @returns {AxiosPromise}
 */
  static requestPostFormDataDownload(headers, data, url) {

  }


  /**
   * 
   * @param {JSON} body 
   * @param {string} url 
   * @returns {AxiosPromise}
   */
  static requestGet(body, url) {

  }

}


export const ExampleComponent = ({ text }) => {}


export const COMMON_CONST = {
  ERROR_CODE: {
    ERROR_FROM_NETWORK: "ERROR_FROM_NETWORK",
    ERROR_FROM_SCREEN_INPUT: "ERROR_FROM_SCREEN_INPUT",
    ERROR_FROM_SCREEN_OUTPUT: "ERROR_FROM_SCREEN_OUTPUT",
    ERROR_FROM_APIOUTPUT: "ERROR_FROM_APIOUTPUT"
  }
}