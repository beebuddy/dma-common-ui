import React from 'react'
import styles from './styles.module.css'
import _Network from './core/actions/network/Network'

export const ExampleComponent = ({ text }) => {
  return <div className={styles.test}>Example Component: {text}</div>
}


export const Network = _Network